"use strict";

function getUser() {
    $.ajax({
        url: "UserDB.php",
        type: "POST",
        data: {
            "getUser": 1
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            if (msg == "") {
                // Guest account
                $("#navbar-top").text("Vieraskäyttäjä");
                $("#navbar-item-logout").hide();
                $("#navbar-item-saved").hide();
                $("#navbar-item-history").hide();

            }
            else {
                $("#navbar-top").text(msg)
                $("#navbar-item-register").hide();

                if(msg == "root"){
                    $("#navbar-item-saved").after("<a href=\"rootactions.php\" class=\"navbar-item\"" +
                        "id=\"navbar-item-root-actions\">Hallinnoi käyttäjiä\n</a>");

                }
            }
        }
    });


}
function logout(){
    $.ajax({
        url: "UserDB.php",
        type: "POST",
        data: {
            "logout": 1
        }, success: function(){
            window.location.href = "LoginPage.php";
        }
    });
}

function getListOfUsers(){
    $.ajax({
        url: "UserDB.php",
        type: "POST",
        data: {
            "getAllUsers": 1
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(errorThrown);
        },
        success: function(json){
            listUsersCallBack(json);
        }
    });
}

function deleteUser(username){
    $.ajax({
        url: "UserDB.php",
        type: "POST",
        data: {
            "deleteUser": username
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(errorThrown);
        },
        success: function(msg){
            console.log(msg);
        }
    });
}