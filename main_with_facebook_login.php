<?php
/**
 * Created by PhpStorm.
 * User: Janne Siivonen
 * Date: 12/14/17
 * Time: 1:04 PM
 * main.php - main page
 */
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <script src="/node_modules/angular/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <meta charset="utf-8">
    <title>FB Login</title>
</head>
<body>

<button id="fblogin">Log in with Facebook</button>
<div id="fb"></div>
<input id="username" type="text" placeholder="Käyttäjätunnus">
<input id="password" type="password" placeholder="Salasana">
<button id="login_button" class="logregbutton"></button>
<button id="register_button" class="logregbutton"></button>

<script>


    function ajaxFBLogin() {
        request = $.ajax({
            url: "./FBLogin.php",
            type: "POST",
            data: 'login',
            dataType: "html",
            error: function (xhr, textStatus, errorThrown) {
                $("#fb").text(errorThrown);
            }
        });
        request.done(function(msg){
            $("#fb").html(msg);
        });
    }



    function login(){

    }

    function register(){

    }

    $(document).ready(function () {
        $("#fblogin").click(function () {
            ajaxFBLogin();
        })
        $("#register_button").click(function () {
            register();
        })

    }



</script>

</body>
</html>