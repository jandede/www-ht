<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/14/17
 * Time: 2:54 PM
 * File FBLogin.php
 */
session_start();
// TODO ???
$_SESSION['first_run'];
$_SESSION['fb'];
$_SESSION['facebook_access_token'];

if(!isset($_SESSION['first_run'])){
    $_SESSION['first_run'] = 1;
}

require_once './vendor/facebook/graph-sdk/src/Facebook/autoload.php';


function loginFB() {

    $_SESSION['fb'] = new Facebook\Facebook([
        'app_id' => 719300364933468,
        'app_secret' => '0e6e0ea04a591c77918d75c6886de9c4',
        'default_graph_version' => 'v2.4',
        'default_access_token' => isset($_SESSION['facebook_access_token']) ? $_SESSION['facebook_access_token'] : '719300364933468|0e6e0ea04a591c77918d75c6886de9c4'
    ]);

    $helper = $_SESSION['fb']->getRedirectLoginHelper();
    $permissions = ['public_profile'];
    $loginUrl = $helper->getLoginUrl('http://localhost/~victor/git/lut-www/ht_angular/FBCallback.php', $permissions);
    trigger_error("access token ".$_SESSION['facebook_access_token']);
    return '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
}
function get_user()
{
    if (isset($_SESSION['facebook_access_token'])) {
        try {
            $response = $_SESSION['fb']->get('/me?fields=id,name', $_SESSION['facebook_access_token']);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $user = $response->getGraphUser();

        trigger_error($user['name']);

        return $user['name'];
    }
    return "access token is null";
}



if (isset($_POST['get_user'])) {
    trigger_error($_SESSION['facebook_access_token']);
    echo get_user();
    // TODO try without return or clean up
    return get_user();
}
if (isset($_POST['loginFB'])) {
    echo loginFB();
}