<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/15/17
 * Time: 12:17 AM
 * File UserDB.php
 * Handle logins/registrations
 */

session_start();
$servername = "localhost";
$username = "abf";
$password = "qwerty";
$dbName = "ht";

// if $_SESSION['logged_user'] does not exist assume guest user
function register($un, $pw)
{
    // Hash and salt
    $pw_hashed = password_hash($pw,PASSWORD_DEFAULT);

    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName) or die("Error " . mysqli_error($conn_sqli));;
    $sql = sprintf("INSERT INTO login (username, password_hash) VALUES ('%s','%s')", $un, $pw_hashed);

    // True == OK
    $result = mysqli_query($conn_sqli, $sql);
    // Log in with the newly created account.
    $_SESSION['logged_user'] = $un;
    mysqli_close($conn_sqli);
    return $result;
}

function getUser(){
    return $_SESSION['logged_user'];
}

// Check the entered username against the database to see if it already exists.
function checkIfNewUser($un){
    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName) or die("Error " . mysqli_error($conn_sqli));;

    $sql = "SELECT username FROM login";
    $result = mysqli_query($conn_sqli, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            if ($un == $row['username']) {
                // User exists!
                return 1;
            }
        }
    }
    mysqli_close($conn_sqli);
    return 0;
}

function FBLogin($name){
    // No need to add to SQL
    trigger_error("Logging in with fb");
    $_SESSION['logged_user'] = $name;
    header('Location: main.php');
}

function login($un, $pw){
    // Guest login
    if($un == "guest" && $pw == null){
        $_SESSION['logged_user'] = "guest";
        return 0;
    }
    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName) or die("Error " . mysqli_error($conn_sqli));;

    $sql = "SELECT username, password_hash FROM login";
    $result = mysqli_query($conn_sqli, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            if ($un == $row['username']) {
                if (password_verify($pw, $row['password_hash'])) {
                    // Username and password are correct.
                    $_SESSION['logged_user'] = $un;
                    mysqli_close($conn_sqli);
                    return 0;
                }

            }
        }
    }
    mysqli_close($conn_sqli);
    // Password is not valid and/or user doesn't exist.
    return 1;
}

function logout(){
    session_destroy();
    trigger_error("LOGOUT");
}

// Used for listing users for root account.
function getAllUsers(){
    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName) or die("Error " . mysqli_error($conn_sqli));;

    $sql = "SELECT username FROM login";
    $result = mysqli_query($conn_sqli, $sql);
    $users = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $users[] = $row;
    }
    mysqli_close($conn_sqli);
    return json_encode($users);
}

// Root can delete users
function deleteUser($usr){
    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName) or die("Error " . mysqli_error($conn_sqli));;

    $sql = "DELETE FROM login WHERE username = '$usr'";
    $result = mysqli_query($conn_sqli, $sql);
    var_dump($result);
    mysqli_close($conn_sqli);
    return $result;
}


if (isset($_POST['checkUser'])) {
     echo checkIfNewUser($_POST['username']);
}

if (isset($_POST['register'])){
    echo register($_POST['username'], $_POST['password']);
}

if(isset($_POST['login'])){
    echo login($_POST['username'], $_POST['password']);
}

if(isset($_POST['getUser'])){
    echo getUser();
}

if(isset($_POST['logout'])){
    logout();
}

if(isset($_POST['getAllUsers'])){
    echo getAllUsers();
}
if(isset($_POST['deleteUser'])){
    echo deleteUser($_POST['deleteUser']);
}

