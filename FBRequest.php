<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/18/17
 * Time: 12:34 AM
 * File FBRequest.php
 */
// create a helper opject which is needed to create a login URL
// the $redirect_login_url is the page a visitor will come to after login
$helper = new FacebookRedirectLoginHelper("http://192.168.31.101/~victor/git/lut-www/ht_angular/main.php");

// First check if this is an existing PHP session
if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
    // create new session from the existing PHP sesson
    trigger_error("TOKEN : " . $_SESSION['fb_token']);
    $session = new FacebookSession( $_SESSION['fb_token'] );
    try {
        // validate the access_token to make sure it's still valid
        if ( !$session->validate() ) $session = null;
    } catch ( Exception $e ) {
        // catch any exceptions and set the sesson null
        $session = null;
        echo 'No session: '.$e->getMessage();
    }
}  elseif ( empty( $session ) ) {
    // the session is empty, we create a new one
    try {
        // the visitor is redirected from the login, let's pickup the session
        $session = $helper->getSessionFromRedirect();
    } catch( FacebookRequestException $e ) {
        // Facebook has returned an error
        echo 'Facebook (session) request error: '.$e->getMessage();
    } catch( Exception $e ) {
        // Any other error
        echo 'Other (session) request error: '.$e->getMessage();
    }
}

if ( isset( $session ) ) {
    // store the session token into a PHP session
    $_SESSION['fb_token'] = $session->getToken();
    // and create a new Facebook session using the cururent token
    // or from the new token we got after login
    $session = new FacebookSession($session->getToken());
}