"use scrict";

function getAvailableLanguages() {
    $.ajax({
        url: "Translator.php",
        type: "POST",
        data: {
            "getAvailableLanguages": 1
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            if (msg == null) {
                // TODO
                ;
            }
            else {
                // Result format:
                // {"dirs": [...], "langs"[..."fi": "Finnish"] }
                var json_result = JSON.parse(msg);
                populateSelectBoxes(json_result.langs);
            }
        }
    });
}

function translate(word, sourceLang, targetLang) {
    $.ajax({
        url: "Translator.php",
        type: "GET",
        data: {
            "translate": 1,
            "word": word,
            "sourceLang": sourceLang,
            "targetLang": targetLang
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            if (msg == null) {
                // TODO
            }
            else {
                // Result format:
                // {"code":###,"lang":"fi-en","text":["translated word"]}
                $("#tr_button").removeClass("is-loading").addClass("is-active");
                var json_result = JSON.parse(msg);
                console.log(json_result);
                // Two possible sources for translation
                if(json_result.translated != null)
                    $("#result_text").text(json_result.translated+"(cached)");
                else
                    $("#result_text").text(json_result.text);
            }

        }
    });
}

function populateSelectBoxes(langs) {
    var $selectBoxSource = $("#source_lang");
    var $selectBoxTarget = $("#target_lang");

    $selectBoxSource.append($("<option />").val(0).text("Lähdekieli"));
    $selectBoxTarget.append($("<option />").val(0).text("Kohdekieli"));
    var i = 0;

    Object.keys(langs).forEach(function (key) {
        $selectBoxSource.append($("<option />").val(key).text(langs[key]));
        $selectBoxTarget.append($("<option />").val(key).text(langs[key]));
        i++;
    });
}

function getHistory() {
    $.ajax({
        url: "Translator.php",
        type: "POST",
        data: {
            "getHistory": 1
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        // Return only current user's history.
        success: function (msg) {
            // Result format:
            // [{"id":"1","username":"un","word":"hei","translated":"hi","source_language":"fi","target_language":"en"},
            console.log(msg);
            var jsonResult = JSON.parse(msg);

            const screenWidth = window.screen.width;
            if (screenWidth < 700) {
                showHistoryMobile(jsonResult);
            }
            else showHistory(jsonResult);
        }
    });
}

function showHistoryMobile(json) {
    var historyTable = $("#history-table-mobile");
    historyTable.empty();

    Object.keys(json).forEach(function (key) {
        historyTable.append("<tr class=\"table-row\" id=\"" + json[key].id + "\"><th>" + json[key].word + "→" + json[key].translated + "  " +
            json[key].source_language + "-" + json[key].target_language + "</th><th><i class=\"fa-star fa\"> </i><i class=\"fa fa-remove\"></i></th>");
    });

    $(document).ready(function () {
        $("#popup-mobile").on("click", ".fa-star", function (e) {
            // Get anchestor <tr> of icon/button so we can determinate its id(== id in translation_history table).
            var nodeID = e.currentTarget.parentElement.parentElement.id;
            saveElement(nodeID);
        });
        $("#popup-mobile").on("click", ".fa-remove", function (e) {
            var nodeID = e.currentTarget.parentElement.parentElement.id;
            e.currentTarget.parentElement.parentElement.remove();
            deleteElement(nodeID);
        });
    });


    var popup = document.getElementById('popup-mobile');
    /*
    popup.style.display = "block";
    */
    $(".popup").fadeIn();

    $(".close").on("click", function () {
        $(".popup").fadeOut();
    });
    $(".pdf-span").on("click", function () {
        // Remove the last column(buttons)
        $("#history-table-mobile th:last-child, #history-table-mobile td:last-child").remove()
        // jQuery selectors don't work here.
        createPDF(document.getElementById("history-table-mobile"));
        // Close dialog
        $(".popup").fadeOut();
    });

    window.onclick = function (event) {
        if (event.target == popup) {
            $(".popup").fadeOut();
        }
    };
}



function showHistory(json) {
    var histTable = $("#history_table");
    histTable.empty();
    $("#dialog_history").dialog({
        width: 'auto',
        popup: true,
        position: ['middle', 20],
        fluid: true,
        'buttons': [{
            text: 'Lataa PDF-tiedostona',
            click: function () {
                $("#history_table th:last-child, #history_table td:last-child").remove()
                // jQuery selectors don't work here.
                createPDF(document.getElementById("history_table"));
                $(this).closest('.ui-dialog-content').dialog('close');
            }
        }]
    }).dialog({show: 'fade'});


        var historyTable = $("#history_table");
    historyTable.append("<thead><th>Alkuperäinen sana</th><th>Käännetty sana</th><th>Alkuperäinen kieli</th>" +
        "<th>Käännöksen kieli</th><th></th></thead>");

    Object.keys(json).forEach(function (key) {
        var buttonLabel = "Tallenna";
        // Light colour for non-saved items. Yellow for saved.
        var buttonClass = "is-light";
        // Check if translation is saved and add button label accordingly.
        if (json[key].saved == 1) {
            buttonLabel = "Tallennettu";
            buttonClass = "is-warning";
        }

        historyTable.append("<tr class=\"table-row\" id=\"" + json[key].id + "\"><th>" + json[key].word +
            "</th><th>" + json[key].translated + "</th><th>" +
            json[key].source_language + "</th><th>" + json[key].target_language + "</th><th><button " +
            "class=\"save_btn button " + buttonClass + "\">" + buttonLabel + "</button><button class=\"delete_btn\">Poista</button></th>");
    });



}


function deleteElement(id) {
    $.ajax({
        url: "Translator.php",
        type: "POST",
        data: {
            "deleteID": id
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            if (msg == 0) {
                console.log("Error");
            } else {
                $("#" + id).remove();
                console.log($("#history_table").html())
            }
        }
    });
}

// Toggle saved state
function saveElement(id) {
    $.ajax({
        url: "Translator.php",
        type: "POST",
        data: {
            "saveID": id
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            console.log(msg);
            if (msg == 0) {
                console.log("Error");
            } else {
                // Only for desktop view.
                console.log("success");
                var btn = $("#" + id).find(".save_btn");
                if (btn.text() == "Tallennettu") {
                    btn.removeClass("is-warning");
                    btn.addClass("is-light");
                    btn.text("Tallenna");
                } else {
                    btn.removeClass("is-light");
                    btn.addClass("is-warning");
                    btn.text("Tallennettu");
                }
            }
        }
    });
}

function getSaved() {
    $.ajax({
        url: "Translator.php",
        type: "POST",
        data: {
            "getSaved": 1
        },
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            console.log("ajax failed" + errorThrown);
        },
        success: function (msg) {
            console.log(msg);
            if (msg == 0) {
                console.log("Error");
            } else {
                var json = JSON.parse(msg);
                showHistory(json);
            }
        }
    });
}



