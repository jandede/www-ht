<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/19/17
 * Time: 7:02 PM
 * File CacheFunctions.php
 */

// Save each translated word to cache, speeding up retrieval.
// Cache is destroyed with the rest of the session upon logout.

define('MEMCACHED_HOST', '127.0.0.1');
define('MEMCACHED_PORT', '11211');

// Create Memcached object
if(!isset($memcache)){
    $memcache = new Memcache;
    $cacheAvailable = $memcache->connect(MEMCACHED_HOST, MEMCACHED_PORT);
}
// Is called when new translations are made.
function addToMem($word, $translated, $sourceLanguage, $targetLanguage){
    global $memcache;
    $translation = array('translated' => $translated, 'sLang' => $sourceLanguage, 'tLang' => $targetLanguage);

    return ($memcache->set($word, $translation));
}

// Check if word exists in Memcached, return null if not.
// Is called when translating(no need to use web API if it's cached)
function getMem($word, $sLang, $tLang){
    global $cacheAvailable, $memcache;
    if($cacheAvailable){
        // Return null if a translation is not found(languages have to match)
        $mem = $memcache->get($word);
        if($mem["sLang"] == $sLang && $mem["tLang"] == $tLang)
            return $mem;
    }
    return null;

}