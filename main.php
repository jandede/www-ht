<?php
/**
 * Created by PhpStorm.
 * User: Janne Siivonen
 * Date: 12/14/17
 * Time: 1:04 PM
 * main.php - main page
 */
?>

<!DOCTYPE html PUBLIC
        "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="en">
<head>
    <meta name="keywords"
          content="translate, translator, english, languages, online translation, online translator, machine translation">

    <meta name="description"
          content="Quick translator with almost 100 languages.">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- PDF creator jspdf -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
    <!-- tool to convert table to pdf printable format -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="UserHandler.js" type="text/javascript"></script>
    <script src="TranslateHandler.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bulma CSS framework -->
    <link rel="stylesheet" type="text/css" href="bulma.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <meta charset="utf-8">
    <title>Online-kääntäjä - Käännä sanoja 100 eri kielelle</title>
</head>
<body>
<!-- CSS popup window for mobile -->
<div id="popup-mobile" class="popup">
    <div class="popup-content">
        <span class="close">&times;</span>
        <table id="history-table-mobile">

        </table>
    </div>

</div>

<!-- Bulma -->
<section class="hero is-dark">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                Kääntäjä
            </h1>
            <h2 class="subtitle">

            </h2>
        </div>
    </div>
</section>
<nav class="navbar" role="navigation" aria-label="dropdown navigation">
    <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link" id="navbar-top">
        </a>
        <div class="navbar-dropdown" id="navbar-top">
            <a class="navbar-item" id="navbar-item-history">
                Käännöshistoria
            </a>
            <a class="navbar-item" id="navbar-item-saved">
                Tallennetut
            </a>
            <a class="navbar-item" id="navbar-item-logout">
                Kirjaudu ulos
            </a>
            <a class="navbar-item" id="navbar-item-register">
                Rekisteröidy/Kirjaudu
            </a>
        </div>
    </div>
</nav>

<div class="tile is-ancestor">
    <div class="tile is-vertical is-3">
        <div class="tile">
            <div class="tile is-parent is-vertical">
                <article class="tile is-child notification is-primary">
                    <div class="control">
                        <div class="select is-primary">
                            <select id="source_lang">

                            </select>
                        </div>
                    </div>
                </article>
                <article class="tile is-child notification is-warning">
                    <div class="control">
                        <div class="select is-warning">
                            <select id="target_lang">

                            </select>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="tile is-vertical is-3">
        <div class="tile is-parent">
            <article class="tile is-child notification is-info">
                <input rows="1" type="text" id="tr_word_input" placeholder="Käännä...">
                <a href="http://translate.yandex.com/">Powered by Yandex Translate</a>
            </article>
        </div>
    </div>
    <div class="tile is-vertical is-2">
        <div class="tile is-parent">
            <article class="tile is-child notification is-success">
                <a class="button is-active is-info" id="tr_button">Käännä</a>
            </article>
        </div>
    </div>
    <div class="tile is-vertical is-3">
        <div class="tile is-parent">
            <article class="tile is-child notification is-danger">
                <h2 id="result_text"></h2>
            </article>
        </div>
    </div>

</div>


<div id="dialog_history" class="dialog-popup">
    <table id="history_table" class="table is-striped is-fullwidth"></table>
</div>


<script>
    "use scrict";
    // TODO Näihin HTML tiedostoihin vain listenerit yms looginen

    function createPDF(div) {

        var doc = new jsPDF('p', 'pt');
        var res = doc.autoTableHtmlToJson(div);
        doc.autoTable(res.columns, res.data);
        doc.save('käännöshistoria.pdf')
    }

    $(document).ready(function () {
        $("#tr_button").on("click", function () {
            $("#source_lang").parent().removeClass("is-danger");
            $("#target_lang").parent().removeClass("is-danger");
            // TODO punaiseksi myös text field


            var word = $("#tr_word_input").val();
            var sourceLang = $("#source_lang").val();
            var targetLang = $("#target_lang").val();
            if (word == null) {
                ;
            }
            if (sourceLang == 0) {
                $("#source_lang").parent().addClass("is-danger");
            }
            if (targetLang == 0) {
                $("#target_lang").parent().addClass("is-danger");
            }
            if (word != null && sourceLang != 0 && targetLang != null) {
                $("#tr_button").removeClass("is-active").addClass("is-loading");
                translate(word, sourceLang, targetLang);
            }
        });

        $("#navbar-item-history").on("click", function () {
            getHistory();
        });
        $("#navbar-item-saved").on("click", function () {
            getSaved();
        });

        $("#history_table").on("click", ".delete_btn", function(e) {
            // ID of table row, ID of history item in SQL.
            var histElementID = e.currentTarget.parentElement.parentElement.id;
            deleteElement(histElementID,e);
        });
        $("#history_table").on("click", ".save_btn", function(e) {
            // ID of table row, ID of history item in SQL.
            var histElementID = e.currentTarget.parentElement.parentElement.id;
            saveElement(histElementID);
        });

        $("#navbar-item-register").on("click",function(){
            window.location.href = "LoginPage.php";
        });

        $("#navbar-item-logout").on("click",function(){
            logout();
        });


        $("#navbar-top").on("click", "#navbar-item-root-actions",function(){
            console.log("root");
            rootPage();
        });


    });

    getUser();

    getAvailableLanguages();

</script>

</body>
</html>