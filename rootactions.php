<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/18/17
 * Time: 3:56 PM
 * File rootactions.php
 */

?>

<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="UserHandler.js" type="text/javascript"></script>
</head>
<body>
    <ul class="list" id="user-list">

    </ul>

<script>

    // TODO Hover ja jotain värejä, otsikko back nappi
    function listUsersCallBack(json){
        var json = JSON.parse(json);

        Object.keys(json).forEach(function (key) {
            if (json[key].username != "root"){
                $("#user-list").append("<li>"+json[key].username+"</li>");
            }
        });
    }

    $(document).ready(function(){
        getListOfUsers();

        $("#user-list").on("click", "li", function(e){
            // Clicked user
           e.currentTarget.firstChild.data;
           e.currentTarget.remove();

           deleteUser(e.currentTarget.firstChild.data);
        });

    });

    request = $.ajax({
        url: "UserDB.php",
        type: "POST",
        data: 'getUser',
        dataType: "html",
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
    request.done(function (msg) {
        // This page is only for root user.
        if (msg != "root") window.location.href = "main.php";
    });



</script>
</body>
</html>
