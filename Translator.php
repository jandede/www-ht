<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/15/17
 * Time: 9:55 PM
 * File Translator.php
 */

session_start();

$servername = "localhost";
$username = "abf";
$password = "qwerty";
$dbname = "ht";

// Requiring this file generates a variable for cached content.
// require_once "CacheFunctions.php";

function translate($word, $sourceLanguage, $targetLanguage)
{
   // $mem = getMem($word, $sourceLanguage, $targetLanguage);
    //if ($mem != null) {
      //  return json_encode($mem);
    //}
    // Yandex API
    $query = sprintf("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20171215T195033Z.5f18".
        "d1b80ebb1df3.e842b15f2d2fbef74b2c08223ba33ae019b92a8b&text=%s&lang=%s-%s&format=plain", $word, $sourceLanguage,
        $targetLanguage);
    $translation = file_get_contents($query);
    $translated = json_decode($translation, true)['text'][0];
    // Add each translation to history(if not cached).
    addToHistory($word, $translated, $sourceLanguage, $targetLanguage);

    return $translation;
}

function getLangs()
{
    $url = "https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=trnsl.1.1.20171215T195033Z.5f18d1b80ebb1df3.e84".
        "2b15f2d2fbef74b2c08223ba33ae019b92a8b&ui=fi";
    // JSON.
    return file_get_contents($url);
}

// Save every translation to history unless it was retrieved from cache.
function addToHistory($word, $translated, $sourceLanguage, $targetLanguage)
{
    global $servername, $username, $password, $dbname;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbname) or die("Error " . mysqli_error($conn_sqli));
    $sql = sprintf("INSERT INTO translation_history (word, translated, username, source_language, target_language)".
        " VALUES ('%s', '%s', '%s', '%s', '%s')", $word, $translated, $_SESSION['logged_user'], $sourceLanguage, $targetLanguage);

    mysqli_query($conn_sqli, $sql);
    mysqli_close($conn_sqli);

    //addToMem($word, $translated, $sourceLanguage, $targetLanguage);
}

// Return every item from history as JSON.
function getHistory()
{
    global $servername, $username, $password, $dbname;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbname) or die("Error " . mysqli_error($conn_sqli));
    $usr = $_SESSION['logged_user'];
    $sql = "SELECT * FROM translation_history WHERE username = '$usr'";

    $result = mysqli_query($conn_sqli, $sql);

    $history = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $history[] = $row;
    }
    // Convert array to JSON.
    return json_encode($history);
}

// jQuery gets id from HTML and passes it as a variable to here.
function deleteElement($id)
{
    global $servername, $username, $password, $dbname;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbname) or die("Error " . mysqli_error($conn_sqli));
    $sql = "DELETE FROM translation_history WHERE id='$id'";
    // 1 == OK, false == NOT OK
    $ret = mysqli_query($conn_sqli, $sql);
    mysqli_close($conn_sqli);
    return $ret;
}

function saveElement($id)
{
    global $servername, $username, $password, $dbname;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbname) or die("Error " . mysqli_error($conn_sqli));
    $sql_select = "SELECT saved FROM translation_history WHERE ID = $id;";
    $result = mysqli_query($conn_sqli, $sql_select);
    // Returns 1 if item is saved, 0 if not.
    $set = 1 ;

    while ($row = $result->fetch_assoc()) {
        if($row['saved'] == 1) $set = 0;
    }
    // Toggle between saved states.
    // If item is saved, set "saved" value to 0 and vice versa.
    $sql = "UPDATE translation_history SET saved = $set WHERE id = $id";
    $ret = mysqli_query($conn_sqli, $sql);
    mysqli_close($conn_sqli);
    // 1 == Success
    return $ret;
}

function getSavedItems()
{
    global $servername, $username, $password, $dbname;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbname) or die("Error " . mysqli_error($conn_sqli));
    $usr = $_SESSION['logged_user'];
    $sql = "SELECT * FROM translation_history WHERE saved = '1' AND username = '$usr'";
    $result = mysqli_query($conn_sqli, $sql);
    mysqli_close($conn_sqli);
    $savedItems = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $savedItems[] = $row;
    }
    return json_encode($savedItems);

}

if (isset($_GET['translate'])) {
    echo translate($_GET['word'], $_GET['sourceLang'], $_GET['targetLang']);
}
if (isset($_POST['getAvailableLanguages'])) {
    echo getLangs();
}

if (isset($_POST['getHistory'])) {
    echo getHistory();
}

if (isset($_POST['deleteID'])) {
    echo deleteElement($_POST['deleteID']);
}
if (isset($_POST['saveID'])) {
    echo saveElement($_POST['saveID']);
}
if (isset($_POST['getSaved'])) {
    echo getSavedItems();
}
