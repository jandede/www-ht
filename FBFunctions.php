<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/19/17
 * Time: 8:29 PM
 * File FBFunctions.php
 */
$redirectURL = "http://ec2-52-58-216-105.eu-central-1.compute.amazonaws.com/ht_v3/main.php";
if(!session_id()) {
    session_start();
}
// From Facebook PHP SDK
require_once 'vendor/autoload.php';
$fb = new Facebook\Facebook([
    'app_id' => 719300364933468,
    'app_secret' => '0e6e0ea04a591c77918d75c6886de9c4',
    'default_graph_version' => 'v2.11',
]);
$helper = $fb->getRedirectLoginHelper();
if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}
$permissions = ['email']; // optional

try {
    if (isset($_SESSION['facebook_access_token'])) {
        $accessToken = $_SESSION['facebook_access_token'];
    } else {
        $accessToken = $helper->getAccessToken();
    }
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
if (isset($accessToken)) {
    if (isset($_SESSION['facebook_access_token'])) {
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    } else {
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        $oAuth2Client = $fb->getOAuth2Client();
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }
    if (isset($_GET['code'])) {
        // TODO Absoluuttiset urlit
        header('Location: $redirectURL');
    }
    try {
        $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
        $profile = $profile_request->getGraphNode()->asArray();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        header("Location: http://ec2-52-58-216-105.eu-central-1.compute.amazonaws.com/ht_v3/LoginPage.php");
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    print_r("Olet kirjautunut käyttäjällä ".$profile['name']);
    require"UserDB.php";
    FBLogin($profile['name']);
} else {
    $loginUrl = $helper->getLoginUrl('http://ec2-52-58-216-105.eu-central-1.compute.amazonaws.com/ht_v3/LoginPage.php', $permissions);
    // echo HTML node to LoginPage.php
    echo '<a href="' . $loginUrl . '">Kirjaudu Facebook-tunnuksilla</a>';
}
