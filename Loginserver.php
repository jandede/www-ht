<?php
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/14/17
 * Time: 6:39 PM
 * File Loginserver.php
 */

$servername = "localhost";
$username = "abf";
$password = "qwerty";
$dbName = "ht";

session_start();

$_SESSION['logged_in'];

function register($un, $pw)
{
    $pw_hashed = hash_pw($pw);

    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName);

    $sql = "INSERT INTO login (username, password) VALUES ('$un','$pw_hashed')";
    $result = mysqli_query($conn_sqli, $sql);
    trigger_error("mysqli return:" . $result . "...", E_USER_NOTICE);
    return $result;
}

function login($un, $pw)
{

    global $servername, $username, $password, $dbName;
    $conn_sqli = mysqli_connect($servername, $username, $password, $dbName);

    $sql = "SELECT username, pwhash FROM login";
    $result = mysqli_query($conn_sqli, $sql);

    if ($pw == null) {
        $user_exists = 0;
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $field = array('username' => $row['username']);
                if ($un == $row['username']) {
                    $user_exists = 1;
                    break;
                }
            }
        }
        return $user_exists;

    } else {
        $loginCorrect = "0";
        $salt = $un;
        $pw_hashed = hash_pw($pw, $salt);
        // Taulukossa on rivit: uid(int, AI), username(text), password(text)
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $field = array('username' => $row['username'], 'pwhash' => $row['pwhash']);
                if ($un == $row['username']) {
                    $realpw = $row['pwhash'];
                    if (password_verify($pw, $realpw)) {
                        $loginCorrect = 1;
                        $_SESSION['logged_in'] = $un;
                    } else {
                        trigger_error("Password is not correct", E_USER_NOTICE);
                    }
                    break;
                }
            }
        }
        return $loginCorrect;
    }
}

function hash_pw($pw)
{
    $pw_hashed = password_hash($pw, PASSWORD_DEFAULT);
    return $pw_hashed;
}

function logout(){
    // Nollaa kaikki $_SESSION muuttujat
    $_SESSION = array();

    return session_destroy();
}


if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['login'])) {
    // 1 => kaikki OK // 0 => käyttäjä/salasana väärä tai jotain muuta ongelmaa
    echo login($_POST['username'], $_POST['password']);
}
if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['register'])) {
    if($_POST['password'] == "checkUser"){
        trigger_error("checkUser", E_USER_NOTICE);
        if (!login($_POST['username'], null)) {
            echo 1; // OK
        } else {
            echo 2;
        }
    }
    // Check if user exists
    else if (!login($_POST['username'], null)) {
        if (register($_POST['username'], $_POST['password'])) {
            // OK
            echo 1;
            // NOT OK
        } else {
            echo 0;
        }
    } else {
        echo 2;
    }

    // Jos JS kyselee käyttäjätietoja ja salasana on checkUser, halutaan vain tarkistaa onko käyttäjä olemassa

}

if (isset($_POST['getUser'])) {
    if (!isset($_SESSION)) { session_start(); }
    if (isset($_SESSION['logged_in']) || $_SESSION['logged_in'] != null) {
        trigger_error("logged user: ".$_SESSION['logged_in'],E_USER_NOTICE);
        echo $_SESSION['logged_in'];
    } else {
        echo "Vieras";
    }
}
if (isset($_POST['logOut'])) {
    echo logout();
}