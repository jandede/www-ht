<!--
/**
 * Created with PhpStorm.
 * User: Janne Siivonen
 * Date: 12/14/17
 * Time: 6:35 PM
 * File LoginPage.php
 */
 -->

<!DOCTYPE html PUBLIC
        "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="stylesheet" type="text/css" href="bulma.css">
    <meta charset="utf-8">
    <title>Kirjaudu sisään tai rekisteröidy</title>
</head>
<body>


<!-- Title bar -->
<section class="hero is-dark">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                Kirjaudu sisään tai rekisteröidy
            </h1>
            <h2 class="subtitle">
                Online-kääntäjä
            </h2>
        </div>
    </div>
</section>

<div class="tile is-ancestor" id="login-page-tile-anchestor is-inline-flex-mobile">
    <div class="tile is-vertical is-3">
        <div class="tile">
            <div class="tile is-parent is-vertical">
                <article class="tile is-child notification is-primary">
                    <input class="login_inputs is-primary " type="text" id="username" placeholder="Käyttäjänimi">
                </article>
            </div>
        </div>
    </div>
    <div class="tile is-vertical is-3">
        <div class="tile is-parent">
            <article class="tile is-child notification is-info ">
                <input class="login_inputs is-info" type="password" id="password" placeholder="Salasana">

            </article>
        </div>
    </div>
    <div class="tile is-vertical is-2">
        <div class="tile is-parent">
            <article class="tile is-child notification is-success">
                <button id="button_login" class="button login_buttons is-danger is-overlay">Kirjaudu</button>
            </article>
        </div>
    </div>
    <div class="tile is-vertical is-2">
        <div class="tile is-parent">
            <article class="tile is-child notification is-danger">
                <button id="button_register" class="button login_buttons is-success">Rekisteröidy</button>
            </article>
        </div>
    </div>

</div>

<div class="tile is-ancestor">
    <div class="tile is-vertical is-11">
        <div class="tile is-parent">
            <article class="tile is-child notification is-link" id="article-error-text">
                <p id="error"></p>
            </article>
        </div>
    </div>
</div>

<p id="error"></p>
<div id="fb">
<?php
   require_once "FBFunctions.php";
?>
</div>

<script>
    function ajaxLogin(un, pw) {
        $.ajax({
            url: "UserDB.php",
            type: "POST",
            data: {
                "login": 1,
                "username": un,
                "password": pw
            },
            dataType: "text",
            error: function (xhr, textStatus, errorThrown) {
                console.log("ajax failed 1" + errorThrown);
            },
            success: function (msg) {
                if (msg == 0) {
                    $("#error").text("Kirjautuminen onnistui");
                    redirect();
                } else {
                    $("#error").text("Kirjautuminen ei onnistunut. Tarkista salasana ja käyttäjätunnus.");
                }
            }
        });
    }

    function ajaxRegister(un, pw) {
        $.ajax({
            url: "UserDB.php",
            type: "POST",
            data: {
                "register": 1,
                "username": un,
                "password": pw
            },
            dataType: "text",
            error: function (xhr, textStatus, errorThrown) {
                console.log("ajax failed 2" + errorThrown);

                $("#error").text("Palvelimeen ei saada yhteyttä.");
            },
            success: function (msg) {
                if (msg == 1) {
                    $("#error").text("Rekisteröiminen onnistui");
                    redirect();
                } else if (msg == 0) {
                    $("#error").text("Rekisteröiminen ei onnistunut");
                    console.log(msg);
                }
            }
        });
    }

    function redirect() {
        window.location.href = "main.php";
    }

    function checkIfExists(username, password) {
        $.ajax({
            url: "UserDB.php",
            type: "POST",
            data: {
                "username": username,
                "checkUser": 1
            },
            dataType: "text",
            error: function (xhr, textStatus, errorThrown) {
                console.log("ajax error :" + errorThrown);
            },
            // Can't return values from ajax call. Further functionality is executed from here. Bad design?
            success: function (msg) {
                if (msg == 0) {
                    ajaxRegister(username, password);
                }
                else if (msg == 1)
                    $("#error").text("Käyttäjä on jo olemassa.");
            }
        });
    }

    function passwordCheck(pw) {
        if (pw.length <= 7) {
            return "Salasanan tulee olla vähinään 8 merkkiä pitkä";
        }
        else if (pw.length >= 256) {
            return "Salasanan pitää olla korkeintaan 256 numeroa pitkä";
        }
        if (/[^a-öA-Ö0-9]/.test(pw)) {
            return "Salasanassa saa olla vain kirjaimia ja numeroita";
        }
        if (pw == pw.toUpperCase() || pw == pw.toLowerCase()) {
            return "Salasanassa tarvitsee olla sekä pieniä että isoja kirjaimia.";
        } else { // Jos menee elseen tiedetään että salasanassa on jo isoja ja pieniä
            var hasNumbers = false;
            for (var i = 0; i < pw.length; i++) {
                if (!isNaN(pw[i])) {
                    hasNumbers = true;
                    break;
                }
            }
            if (!hasNumbers) {
                return "Salasanassa tarvitsee olla myös numeroita.";
            }
        }
        return 0;
    }

    // TODO jahka fnuktiot js filessä, käytä samaa funktiota molempiin kutsuihin
    function logout() {
        $.ajax({
            url: "UserDB.php",
            type: "POST",
            data: {
                "logout": 1
            }
        });
    }

    $(document).ready(function () {
        $("#button_login").on("click", function () {
            if ($("#username").val().length === 0 || $("#password").val().length === 0) {
                // Log in as guest
                window.location.href = "main.php";
            } else {
                ajaxLogin($("#username").val(), $("#password").val());
            }
        });

        $("#button_register").on("click", function () {
            var unVal = $("#username").val();
            if (unVal.length === 0 || $("#password").val().length === 0) {
                $("#error").text("Virheelliset tiedot.");
            }
            else {
                var pw_check = passwordCheck($("#password").val());
                if (pw_check == 0) {
                    checkIfExists(unVal, $("#password").val());
                } else {
                    $("#error").text(pw_check);
                }
            }

        });

        $("#error").text("Kirjaudu sisään tai rekisteröi uusi käyttäjä. Voit myös kirjautua vieraskäyttäjänä jos et aseta" +
            "käyttäjätunnusta ja salasanaa.");

    });
    // Login page is accessed. Logged users will be logged out.
    logout();

</script>


</body>
</html>